#
# Be sure to run `pod lib lint Short_Test.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
    s.name             = 'Short'
    s.version          = '0.1.0'
    s.summary          = 'Siri Shortcut Wrapper'

    # This description is used to generate tags and improve search results.
    #   * Think: What does it do? Why did you write it? What is the focus?
    #   * Try to keep it short, snappy and to the point.
    #   * Write the description between the DESC delimiters below.
    #   * Finally, don't worry about the indent, CocoaPods strips it!

    s.description      = 'Tiny wrapper that helps you to easily manage Siri Shortcuts with few lines of code'

    s.homepage         = 'https://bitbucket.org/soledue/short'
    # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
    s.license          = { :type => 'MIT', :file => 'LICENSE' }
    s.author           = { 'soledue' => 'i.kanev@reply.it' }
    s.source           = { :git => 'https://soledue@bitbucket.org/soledue/short.git', :tag => s.version.to_s }
    # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

    s.ios.deployment_target = '9'
    s.swift_version = '4.0'

    s.source_files = 'Short/Classes/**/*'

    # s.resource_bundles = {
    #   'Short_Test' => ['Short/Assets/*.png']
    # }

    s.frameworks = 'Intents', 'IntentsUI'

end

# Short

![Swift 4.x](https://img.shields.io/badge/swift-4.x-EF5138.svg)
![Under MIT License](https://img.shields.io/badge/license-MIT-blue.svg)
[![Platform](https://img.shields.io/badge/Platform-iOS-lightgrey.svg)](https://developer.apple.com/)

## Requirements
- iOS 12
- Xcode 10

## Installation

Short is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'Short'
```

## Usage
```swift
import Short
```
#### Getting All Voice Shortcuts
```swift
Short.getAllVoiceShortcuts { shortcuts in
    for shortcut in shortcuts {
        print(shortcut)
    }
}
```
#### Getting Voice Shortcut from an Intent
````swift
let intent = INIntent()
Short.voiceShortcutFrom(intent: menu!.intent) { voiceShortcut in
    print(voiceShortcut)
}
````
#### Getting Shortcut from an Intent
````swift
let intent = INIntent()
let shortcut = Short.shortcut(from: intent)
````
#### Deleting All Donations
````swift
Short.removeAllDonate { error in
    print(error)
}
````
#### Adding Phases to Siri from Indside a ViewController
````swift
let intent = INIntent()
if let shotcut = Short.shortcut(from: intent) {
    Short.addSiriShortcut(from: self, shortcut: shotcut) { voiceShortcut, error in
        print(voiceShortcut, error)
    }
}
````
#### Editting Siri Phrase from Indside a ViewController
````swift
Short.editSiriShortcut(from: self, shortcut: shortcut) { voiceShortcut, uuid, error in
    if let voiceShortcut = voiceShortcut {
        print("phrase has modificated")
    } else if uuid != nil {
        print("phrase has deleted")
    }
}
````
#### Donate Intent Intaration
````swift
let intent = INIntent()
Short.donateInteraction(for: intent) { error in
    print(error)
}
````

## Author

i.kanev@reply.it

## License

Short is available under the MIT license. See the LICENSE file for more info.

//
//  Short.swift
//  Short
//
//  Created by Ivo Kanev on 14/06/2018.
//  Copyright © 2018 Reply SPA. All rights reserved.
//

import Foundation
import Intents
import IntentsUI

@available(iOS 12.0, *)
public final class Short: NSObject {
    private static let shared = Short()
    private weak var presentVC: UIViewController?
    private var presentAnimated = false
    private var callbackAdd: ((INVoiceShortcut?, Error?) -> Void)?
    private var callbackEdit: ((INVoiceShortcut?, UUID?, Error?) -> Void)?
    public class func allVoiceShortcuts(completion: @escaping ([INVoiceShortcut]?) -> Void) {
        INVoiceShortcutCenter.shared.getAllVoiceShortcuts { voiceShortsCuts, error in
            DispatchQueue.main.async {
                guard let voiceShortsCuts = voiceShortsCuts else {
                    completion(nil)
                    return
                }
                completion(voiceShortsCuts)
            }
        }
    }
    public class func voiceShortcutFrom(intent: INIntent, completion: @escaping (INVoiceShortcut?) -> Void) {
        guard let identifier = intent.identifier, let uuid = UUID(uuidString: identifier) else {
            completion(nil)
            return
        }
        INVoiceShortcutCenter.shared.getVoiceShortcut(with: uuid) { voiceShortCut, error in
            guard let voiceShortCut = voiceShortCut else {
                completion(nil)
                return
            }
            completion(voiceShortCut)
        }
    }
    public class func shortcut(from intent: INIntent) -> INShortcut? {
        return INShortcut(intent: intent)
    }
    public class func addSiriShortcut(from vc: UIViewController,
                                      shortCut: INShortcut,
                                      animated: Bool = true,
                                      completion: ((INVoiceShortcut?, Error?) -> Void)? = nil) {
        shared.addSiriShortcut(from: vc, shortCut: shortCut, animated: animated, completion: completion)
    }
    public class func editSiriShortCut(from vc: UIViewController,
                                      shortcut: INVoiceShortcut,
                                      animated: Bool = true,
                                    completion: ((INVoiceShortcut?, UUID?, Error?) -> Void)? = nil) {
        shared.editSiriShortcut(from: vc, shortCut: shortcut, completion: completion)
    }
    public class func removeAllDonate(completion: ((Error?)->Void)? = nil) {
        INInteraction.deleteAll { error in
            completion?(error)
        }
    }
    public class func donateInteraction(for intent: INIntent, completion: ((Error?) -> Void)? = nil) {
        let interaction = INInteraction(intent: intent, response: nil)
        interaction.donate { error in
            if let error = error {
                print(error)
            } else {
                print("success")
            }
        }
    }
    public class func remove(identity: String, completion: ((Error?) -> Void)? = nil) {
        INInteraction.delete(with: identity) { error in
            completion?(error)
        }
    }
}
@available(iOS 12.0, *)
extension Short {
    public func addSiriShortcut(from vc: UIViewController,
                                shortCut: INShortcut,
                                animated: Bool = true,
                                completion: ((INVoiceShortcut?, Error?) -> Void)? = nil) {
        let shortCutVC = INUIAddVoiceShortcutViewController(shortcut: shortCut)
        shortCutVC.delegate = self
        presentVC = vc
        presentAnimated = animated
        callbackAdd = completion
        vc.present(shortCutVC, animated: animated, completion: nil)
    }
    public func editSiriShortcut(from vc: UIViewController,
                                 shortCut: INVoiceShortcut,
                                 animated: Bool = true,
                                 completion: ((INVoiceShortcut?, UUID?, Error?) -> Void)? = nil) {
        let shortCutVC = INUIEditVoiceShortcutViewController(voiceShortcut: shortCut)
        shortCutVC.delegate = self
        presentVC = vc
        presentAnimated = animated
        callbackEdit = completion
        vc.present(shortCutVC, animated: animated, completion: nil)
    }
}
@available(iOS 12.0, *)
extension Short: INUIAddVoiceShortcutViewControllerDelegate {
    public func addVoiceShortcutViewController(_ controller: INUIAddVoiceShortcutViewController, didFinishWith voiceShortcut: INVoiceShortcut?, error: Error?) {
        if let error = error as NSError? {
            print("siri shortcur error %@", error.localizedDescription)
        } else {
            print("siri shortcur added")
        }
        presentVC?.dismiss(animated: presentAnimated, completion: nil)
        callbackAdd?(voiceShortcut, error)
    }

    public func addVoiceShortcutViewControllerDidCancel(_ controller: INUIAddVoiceShortcutViewController) {
        print("siri shortcur cancelled")
        presentVC?.dismiss(animated: presentAnimated, completion: nil)
    }
}
@available(iOS 12.0, *)
extension Short: INUIEditVoiceShortcutViewControllerDelegate {
    public func editVoiceShortcutViewController(_ controller: INUIEditVoiceShortcutViewController, didUpdate voiceShortcut: INVoiceShortcut?, error: Error?) {
        presentVC?.dismiss(animated: presentAnimated, completion: nil)
        callbackEdit?(voiceShortcut, nil, error)
    }

    public func editVoiceShortcutViewController(_ controller: INUIEditVoiceShortcutViewController, didDeleteVoiceShortcutWithIdentifier deletedVoiceShortcutIdentifier: UUID) {
        presentVC?.dismiss(animated: presentAnimated, completion: nil)
        callbackEdit?(nil, deletedVoiceShortcutIdentifier, nil)
    }

    public func editVoiceShortcutViewControllerDidCancel(_ controller: INUIEditVoiceShortcutViewController) {
        print("siri shortcur cancelled")
        presentVC?.dismiss(animated: presentAnimated, completion: nil)
        callbackEdit?(nil, nil, nil)
    }

}
